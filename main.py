from typing import Optional
from fastapi import FastAPI
import psycopg2

import base64
from io import BytesIO
from matplotlib.figure import Figure

def run_query(query):
    try:
        conn = psycopg2.connect(database="nba", user="", password="", host="127.0.0.1", port="5432")
        cur = conn.cursor()
        cur.execute(query)
        rows = cur.fetchall()
        return rows
    except Exception as e:
        if conn:
            conn.rollback()
        print(f"Error: {e}")
    finally:
        if conn:
            conn.close()

app = FastAPI()

@app.get("/")
def read_root():
    return {}

# api - conferences
@app.get("/api/conferences")
def conferences():
    conferences = run_query("select distinct conference from teams order by conference;")
    return {'data': conferences}

# api - divisions
@app.get("/api/divisions")
def divisions():
    divisions = run_query("select distinct division from teams order by division;")
    return {'data': divisions}

# api - all teams
@app.get("/api/teams")
def teams():
    teams = run_query("select * from teams order by full_name;")
    return {'data': teams}

# api - team by name
@app.get("/api/teams/{team_name}")
def team_by_name(team_name: str):
    team = run_query(f"select * from teams where name ilike '{team_name}' order by name;")
    return {'data': team}

# api - teams by division
@app.get("/api/teams/division/{division_type}")
def teams_by_division(division_type: str):
    teams = run_query(f"select * from teams where division ilike '{division_type}' order by full_name;")
    return {'data': teams}

# api - teams by conference
@app.get("/api/teams/conference/{conference_type}")
def teams_by_conference(conference_type: str):
    teams = run_query(f"select * from teams where conference ilike '{conference_type}' order by full_name;")
    return {'data': teams}        

# @TODO:
# api - add a team
@app.post("/api/teams/team")
def add_team():
    return

# @TODO:
# api - update a team
@app.put("/api/teams/team")
def update_team():
    return

# @TODO:
# bar chart - num teams by conference
@app.get("/charts/conference")
def chart_conference():
    # Generate the figure **without using pyplot**.
    fig = Figure()
    ax = fig.subplots()
    ax.plot([1, 2])
    # Save it to a temporary buffer.
    buf = BytesIO()
    fig.savefig(buf, format="png")
    # Embed the result in the html output.
    data = base64.b64encode(buf.getbuffer()).decode("ascii")
    return f"<img src='data:image/png;base64,{data}'/>"

# @TODO:
# bar chart - num teams by division
@app.get("/charts/division")
def chart_division():
    # Generate the figure **without using pyplot**.
    fig = Figure()
    ax = fig.subplots()
    ax.plot([1, 2])
    # Save it to a temporary buffer.
    buf = BytesIO()
    fig.savefig(buf, format="png")
    # Embed the result in the html output.
    data = base64.b64encode(buf.getbuffer()).decode("ascii")
    return f"<img src='data:image/png;base64,{data}'/>"

