import json
import csv

with open('teams.json') as json_file:
    data = json.load(json_file)

team_data = data['teams']

data_file = open('teams.csv', 'w')

csv_writer = csv.writer(data_file)

count = 0

for team in team_data:
    if count == 0:
        header = team.keys()
        csv_writer.writerow(header)
        count += 1
    csv_writer.writerow(team.values())

data_file.close()